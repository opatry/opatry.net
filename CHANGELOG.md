# Changelog

## [2.4.0] 2017-11-25:
### Added
 - Support for GitLab pipelines
 - New note about Automated Firebase hosting and BitBucket/GitLab pipelines

## [2.3.0] 2017-11-12:
### Added
 - Support for BitBucket pipelines
 - Firebase hosting
 - New notes (BitBucket, Android Studio…)

## [2.2.0] 2017-05-20:
### Added
 - Nebo in presentation
### Changed
 - Shellcheck in scripts

## [2.1.0] 2016-03-02:
### Removed
 - Removed notes as they weren't super useful
### Changed
 - Updated presentation

## [2.0.1] 2015-01-04:
### Changed
 - Style improvements

## [2.0.0] 2014-04-26:
### Changed
 - Migrated from Jekyll to Nanoc

## [1.0.0] 2014-04-10:
### Addded
 - Jekyll version