# Olivier Patry

The content is, most of the time, written using [Markdown](http://daringfireball.net/projects/markdown/) and served by [`nanoc`](http://nanoc.ws/).

This website is hosted on https://opatry.net. 
