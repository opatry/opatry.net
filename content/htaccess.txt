---
layout: none
---
ErrorDocument 404 /error/404/

<IfModule mod_expires.c>
ExpiresActive On
ExpiresByType image/jpg "access 1 year"
ExpiresByType image/jpeg "access 1 year"
ExpiresByType text/css "access 1 month"
ExpiresByType text/html "access 1 month"
ExpiresByType application/pdf "access 1 year"
ExpiresByType application/x-javascript "access 1 month"
ExpiresByType text/javascript "access 1 month"
ExpiresByType application/javascript "access 1 month"
ExpiresByType image/x-icon "access 1 year"
ExpiresByType image/icon "access 1 year"
ExpiresByType application/x-ico "access 1 year"
ExpiresByType application/ico "access 1 year"
ExpiresDefault "access 1 month"
</IfModule>

<ifmodule mod_deflate.c>
<ifmodule mod_mime.c>
Addtype font/opentype .otf
Addtype font/eot .eot
Addtype font/truetype .ttf
</ifmodule>
AddOutputFilterByType DEFLATE text/html text/plain text/xml text/css application/x-javascript application/javascript text/text
AddOutputFilterByType DEFLATE font/opentype font/truetype font/eot
AddOutputFilterByType DEFLATE image/x-icon
</ifmodule>
