---
title: Home
kind: resume
---
You are on the personal website of Olivier Patry, software engineer working for [MyScript](http://developer.myscript.com/) (handwriting recognition for text, math, geometry, diagram, music…).

Specialized in software **architecture** and **object oriented** programming for **mobile/embedded devices** mainly using
**C++** and **Java** (Android &amp; Desktop).

I'm currently working as a team leader on [Nebo](http://myscript.com/nebo/), MyScript's note taking application.

<div style="margin: 0 auto; width: 560px;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/2c1oeYGmig0?autoplay=0&amp;rel=0" frameborder="0" allowfullscreen=""></iframe>
</div>

----

<div class="special-links" markdown="1">
[<span class="icon-github"></span>](https://github.com/opatry)
[<span class="icon-feed"></span>](/rss.xml)
</div>

----

## Notes
<% notes.each do |note| %>
* [<%= note[:title] %>](<%= note.path %>)
<% end %>